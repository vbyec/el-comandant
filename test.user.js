/*
ToDo 
1. Подменять кривое время начала форта на нормальное
2. в списке фортов выводить время до начала боя

время выводимое в весте= GMT+1 - время по берлину
GMt=west_time-1
для русского GMT.localtime
*/



var page;
$.ajax({
    type: "POST",
    url: "game.php?window=fort_overview"
}).done(function(data) {
    page = data.page;
})

$(page).find("span[class^='fortBattle']").each(function() {
    day = $(this).text().split(':')[1].split(' ')[1];
    hours = $(this).text().split(':')[1].split(' ')[3];
    minutes = $(this).text().split(':')[2]
    console.log(day + ' ' + hours + ':' + minutes);
})
//Получаем "сегодня" или "завтра"
//$($(page).find("span[class^='fortBattle']")[0]).text().split(':')[1].split(' ')[1]

//Получаем часы
//$($(page).find("span[class^='fortBattle']")[0]).text().split(':')[1].split(' ')[3]

//Получаем минуты
//$($(page).find("span[class^='fortBattle']")[0]).text().split(':')[2]

//Возможно не нужно будет .replace(/\D+/g, "");

function convert_time(day, hour, minute) {
    switch (day) {
        case "сегодня":
            day = new Date().getDate();
            break
        case "завтра":
            day = new Date(new Date().setDate(new Date().getDate() + 1)).getDate();
            break
    }
}

























FortOverviewWindow.createContent=
function(tab, options) {
    if (options !== undefined) {
        for (var el in options) this.options[el] = options[el];
    }
    Ajax.remoteCall('fort_overview', '', {
        offset: this.options.offset ? this.options.offset : 0
    }, function(json) {
        $('div.tw2gui_window_content_pane #fo_hiddenContent', FortOverviewWindow.DOM).remove();
        $('div.tw2gui_window_content_pane', FortOverviewWindow.DOM).append('<span id="fo_hiddenContent" style="display:none;">' + json["page"] + '</span>');
        $('img.fortOverviewIconScroll', FortOverviewWindow.DOM).attr('title', '<span class="text_bold">' + 'Показать форт на карте' + '</span>');
        $('img.fortOverviewIconScrollTown', FortOverviewWindow.DOM).attr('title', '<span class="text_bold">' + 'Показать на карте' + '</span>');
        if (json["js"]) {
            for (var i in json["js"]) {
                var e = json["js"][i];
                $('.wayTime' + e[0], FortOverviewWindow.DOM).each(function(k, el) {
                    el.innerHTML = Map.calcWayTime(Character.getPosition(), {
                        x: e[1],
                        y: e[2]
                    }).formatDuration();
                });
                if (!e[3]) continue;
                $('.fortBattle' + e[0], FortOverviewWindow.DOM).each(function(k, el) {
                    $(el).css('cursor', 'help');
                    if (e[5])
                        $(el).attr('title', '<strong>' + s('Нападающий: %1 из %2', (e[4]).escapeHTML(), (e[5]).escapeHTML()) + '</strong>');
                    else
                        $(el).attr('title', '<strong>' + s('Нападающий: %1', (e[4]).escapeHTML()) + '</strong>');
                });
            }
        }
        FortOverviewWindow.Maneuver.infoShown = false;
        FortOverviewWindow.OwnForts.infoShown = false;
        FortOverviewWindow.RecentBattles.infoShown = false;
        FortOverviewWindow.FortSearch.infoShown = false;
        FortOverviewWindow.showTab(tab);
    });
}