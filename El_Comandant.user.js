// ==UserScript==
// @name TW
// @description Tweaker
// @author Vbyec
// @license MIT
// @version 1.0
// @include http://*.the-west.*/game.php*
// ==/UserScript==
/* ToDo 
Печатать текст в активный чат

*/

function exec(fn) {
    var script = document.createElement('script');
    script.setAttribute("type", "application/javascript");
    script.textContent = '(' + fn + ')();';
    document.body.appendChild(script); // run the script
    document.body.removeChild(script); // clean up
}

exec(function() {
    window.Players = {};
    Players.MyID = Chat.MyId.split('_')[1];

    Players.add_name = function(id, name) {
        localStorage.setItem("PlayerId_" + id, name);
        return true;
    };

    Players.get_name = function(id) {
        return localStorage.getItem("PlayerId_" + id);
    };

    Players.get_id = function(name) {
        for (var key in localStorage) {
            index = key.split('_')[1];
            if (Players.get_name(index) == name) {
                return index;
            }
        }
    };

    Players.init = function() {
        if (Players.get_name(1) === "test") {
            console.log("already created");
        } else {
            Players.get_data();
        }
    }

    Players.get_data = function() {

        var add_names = function(data) {
            data.ranking.forEach(function(player) {
                Players.add_name(player.player_id, player.name);
            });
        };

        $.ajax({
            type: "POST",
            url: "game.php?window=ranking&mode=get_data",
            data: {
                page: 1,
                tab: 'experience'
            }
        })
            .done(function(data) {
                var pages = data.pages;
                for (var i = 1; i < pages; i++) {
                    $.ajax({
                        type: "POST",
                        url: "game.php?window=ranking&mode=get_data",
                        data: {
                            page: i,
                            tab: 'experience'
                        }
                    })
                        .done(add_names);
                }
            });
        Players.add_name(1, "test");
    };



    //Можно ли использовать arguments?
    FortBattle.flashShowCharacterInfoOrigin = FortBattle.flashShowCharacterInfo;
    FortBattle.flashShowCharacterInfo = function(fortId, playerId, healthNow, healthMax, totalDmg, lastDmg, shotat, bonusdata) {
        FortBattle.flashShowCharacterInfoOrigin(fortId, playerId, healthNow, healthMax, totalDmg, lastDmg, shotat, bonusdata);
        FortBattle.flashShowCharacterInfoEl(fortId, playerId, healthNow, healthMax, totalDmg, lastDmg, shotat, bonusdata);
    };

    FortBattle.flashShowCharacterInfoEl = function(fortId, playerId, healthNow, healthMax, totalDmg, lastDmg, shotat, bonusdata) {
        if (Players.MyID === playerId) return;
        setTimeout(function() {
            document['onkeyup'] = null;
        }, 2500);
        document['onkeyup'] = function(e) {
            e = e || window.event;
            e.preventDefault();

            var nick = Players.get_name(playerId);
            var function_17 = function(nick) { /* ctrl */
                $('input.message').first().val($('input.message').val() + "*" + nick + "* ");
                $('input.message').first().focus();
            };

            var function_16 = function(nick) { /* shift */
                $('input.message').first().val($('input.message').val() + "*" + nick + "* смена с  ");
            };

            var keyCode = e.keyCode ? e.keyCode : e.charCode;

            if ([17, 16, 36, 90].indexOf(keyCode) >= 0) {
                eval("function_" + keyCode + "('" + nick + "');");
            }

            document['onkeyup'] = null; /*smth*/
        };
    };

    function log(msg) {
        console.log(msg);
    }

    function show_topic() {
        var room = Chat.Resource.Manager.getRoom('room_town_' + Chat.Resource.Manager.getClient(Chat.MyId).townId);
        //todo перепиcать на чистый js
        $("<p style='left: 50%; position: absolute; top: 44px; margin-left: -305px; color: white; font-weight: bolder; font-size: 18px;'>" + room.topic + "</p>").appendTo("#ui_topbar");
    };

    //wtf? setTimeout? really?
    timeout_id = window.setTimeout(function() {
        show_topic();
    }, 10000);
});